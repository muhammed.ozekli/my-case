#!/bin/bash


#creating monitoring namespace
kubectl apply -f /root/my-case/prometheus/monitoring_namespaces.yml
sleep 3
#create clusterrole
kubectl apply -f /root/my-case/prometheus/clusterrole.yml  --namespace=monitoring
sleep 2
#create service account
kubectl apply -f /root/my-case/prometheus/service_account.yml --namespace=monitoring
sleep 2
#binding cluster role to service account
kubectl apply -f /root/my-case/prometheus/clusterrole_binding.yml --namespace=monitoring
sleep 2
#creating configmap from prometheus yaml file
kubectl apply -f /root/my-case/prometheus/scrape.yml --namespace=monitoring
sleep 2
#deploy the internal prometheus
#kubectl apply -f /root/my-case/prometheus/prometheus_deployment.yml --namespace=monitoring
kubectl apply -f /root/my-case/prometheus/prometheus_deployment_withaffinity.yml --namespace=monitoring

sleep 2
#expose prometheus ui to public with nodeport
kubectl apply -f /root/my-case/prometheus/preometheus_service.yml --namespace=monitoring



kubectl label node node2 mamilabel=mamiworkernode
kubectl taint nodes node2 mamilabel=mamiworkernode:NoSchedule

# to untaint a node : just add "-" at the end of the line
#kubectl taint nodes node2 mamilabel=mamiworkernode:NoSchedule-

#kubectl convert -f my-case/prometheus/prometheus_deployment.yml --output-version apps/v1

#kubectl apply -f /root/my-case/prometheus/prometheus_deployment_withaffinity.yml --namespace=monitoring



from kubernetes import client, config
import json
import nginx
#import sys
import ast
#exposedport = sys.argv[1]

def nginxgenerator(svcname, endpoint, name, exposedport):
    c = nginx.Conf()
#    endpoint = enumerate(endpoint)
#    print(type(endpoint))
    endpoint = ast.literal_eval(endpoint)
    #endpoint = "http://"+endpoint
    c.add(
    nginx.Key('#user', 'nginx'),
    nginx.Key('error_log', '/var/log/nginx/error.log')
       )

    if len(endpoint) == 1:
        u = nginx.Upstream(f'{name}',
            nginx.Key('server', f'{endpoint[0]}')
            )
    if len(endpoint) == 2:
        u = nginx.Upstream(f'{name}',
            nginx.Key('server', f'{endpoint[0]}'),
            nginx.Key('server', f'{endpoint[1]}'),
            )
    if len(endpoint) == 3:
        u = nginx.Upstream(f'{name}',
            nginx.Key('server', f'{endpoint[0]}'),
            nginx.Key('server', f'{endpoint[1]}'),
            nginx.Key('server', f'{endpoint[2]}')
            )
    if len(endpoint) >= 4:
        u = nginx.Upstream(f'{name}',
            nginx.Key('server', f'{endpoint[0]}'),
            nginx.Key('server', f'{endpoint[1]}'),
            nginx.Key('server', f'{endpoint[2]}'),
            nginx.Key('server', f'{endpoint[3]}')
            )

    if len(endpoint) >4:
        print("this nginx config generater supports max 4 nodes in kubernetes cluster, to change it, you gotta modify this script")

    c.add(u)
    s = nginx.Server()
    s.add(
    nginx.Key('listen', f'{exposedport}'),
#    nginx.Key('listen', '85'),
    nginx.Comment(f'{svcname}'),
    nginx.Key('server_name', 'localhost 0.0.0.0'),

    nginx.Location('= /',
    nginx.Key('proxy_pass', f'http://{name}'),
    nginx.Key('log_not_found', 'off'),
    nginx.Key('access_log', 'on')
        )
    #nginx.Location('~ \.php$',
    #nginx.Key('proxy_pass', f'{endpoint}'),
    #nginx.Key('fastcgi_intercept_errors', 'on'),
    #nginx.Key('fastcgi_pass', 'php')
    #    )
    )
    c.add(s)
    nginx.dumpf(c, f'confs_ng/{name}.conf')


# Configs can be set in Configuration class directly or using helper utility
config.load_kube_config()

v1 = client.CoreV1Api()
print("Listing service name which has hayde.trendyol.io/enabled  anotations:")
#a = v1.list_endpoints_for_all_namespaces(watch=False)
#b = v1.list_service_for_all_namespaces_with_http_info(watch=False)
b = v1.list_service_for_all_namespaces(watch=False)
a = v1.list_node(watch=False)

nodes = []
for node in a.items:
    nodes.append(node.status.addresses[0].address)
print(f"nodes={nodes}")

exposedservices = []
for ii in b.items:
    if ii.metadata.annotations is not None:
        if ii.spec.ports[0].node_port is not None:

            if('hayde.trendyol.io/enabled' in ii.metadata.annotations):
               exposedservices.append(ii.metadata.name)
print(f"exposed={exposedservices}")
services = {}
svcnames = []
portss = {}
sc = 0
while  sc < len(exposedservices):
    for ii in b.items:
#        print(ii.metadata.name)
        if ii.metadata.name == exposedservices[sc]:
            exposeport = ii.spec.ports[0].node_port
            svname = ii.metadata.name
            portss[svname] = exposeport
            xp = exposedservices[sc]
            xp = {}
            sn = []
            nd = 0
            while nd < len(nodes):
                endpoint = nodes[nd] + ":" + str(ii.spec.ports[0].node_port)
                servicenm = ii.metadata.name + str(nd)
                name = ii.metadata.name
                sn.append(servicenm)
                xp[servicenm] = endpoint
                nd += 1
            services[name] = xp
            svcnames.append(sn)
#            print(xp)
#            print(sn)
    sc += 1
print(services)
print(f"ports = {portss}")
#print(svcnames)
#services = {}
#svcnames = []
#for ii in b.items:
#    if ii.metadata.annotations is not None:
#        if ii.spec.ports[0].node_port is not None:

#            if('hayde.trendyol.io/enabled' in ii.metadata.annotations):
#                print(ii)
#                nd = 0
#                while nd < len(nodes):
#                    endpoint = nodes[nd] + ":" + str(ii.spec.ports[0].node_port)
#                    servicenm = ii.metadata.name + str(nd)
#                    name = ii.metadata.name
#                    svcnames.append(servicenm)
#                    services[servicenm] = endpoint
#                    nd += 1

#print(services)
svccount = len(exposedservices)
#print(svccount)
ob = 0
#svclist = []
while ob < svccount:
    servicename = exposedservices[ob]
    svcs = services.get(f"{servicename}")
    print(f"svcs={svcs}")
#    print(len(svcnames[ob]))
    ends = []
    ds = 0
    while ds < len(svcnames[ob]):
        ind = servicename + str(ds)
        endss = svcs.get(f"{ind}")
        ends.append(endss)
        ds += 1
    exposedport = portss.get(f"{servicename}")
    print(f"exposedport={exposedport}")
    print(f"ends={ends}")
    nginxgenerator(f"{servicename}", f"{ends}", f"{servicename}", f"{exposedport}")
    ob += 1
#    nginxgenerator(f"{servicename}", f"{svcnames}", f"{name}")
#    print(svcendpoint)
#    svclist.append(svcendpoint)
#    ob += 1
#print(svclist)
#print(svclist[0])
#svclist = list(svclist)
#nginxgenerator(f"{servicename}", f"{svclist}", f"{name}", f"{exposedport}")

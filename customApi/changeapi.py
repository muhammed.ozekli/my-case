from kubernetes import client, config
import json
#import datetime
from datetime import datetime, timedelta
import pytz
utc=pytz.UTC

y = datetime.utcnow()
y = utc.localize(y)
#t1 = timedelta(seconds=-8)
t2 = timedelta(minutes=-2)
y = y + t2
#x = datetime.datetime.now()
#print(y)
# Configs can be set in Configuration class directly or using helper utility
config.load_kube_config()

v1 = client.CoreV1Api()
#a = v1.list_endpoints_for_all_namespaces(watch=False)
#b = v1.list_service_for_all_namespaces_with_http_info(watch=False)
b = v1.list_service_for_all_namespaces(watch=False)
a = v1.list_node(watch=False)

nodes = []
for node in a.items:
    nodes.append(node.status.addresses[0].address)

exposedservices = []
for ii in b.items:
    if ii.metadata.annotations is not None:
        if ii.spec.ports[0].node_port is not None:

            if('hayde.trendyol.io/enabled' in ii.metadata.annotations):
#                print(ii.metadata.managed_fields[0].time)
                mdftime = ii.metadata.managed_fields[0].time
#                mdftime = utc.localize(mdftime)
                if mdftime >= y:
                    print("modified")

                exposedservices.append(ii.metadata.name)
#!/bin/bash

#create configMap
kubectl create -f grafana-datasource-config.yaml

#create deployment
kubectl create -f grafana-deployment.yaml

#create service
kubectl create -f grafana-service.yaml


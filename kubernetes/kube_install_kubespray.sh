#!/bin/bash
node1 = ${1}
node2 = ${2}

# Update Ansible inventory file with inventory builder
declare -a IPS=($node1 $node2)
CONFIG_FILE=inventory/case-muhammet-ozekli.abc/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}

#vim inventory/case-muhammet-ozekli.abc/hosts.yaml



cd /root/kubespray
ansible-playbook -i inventory/case-muhammet-ozekli.abc/hosts.yaml cluster.yml -b -v --private-key=../.ssh/id_rsa

kubectl label node node2 node-role.kubernetes.io/master-
kubectl label node node2 node-role.kubernetes.io/worker=worker
kubectl label node node1 node-role.kubernetes.io/worker=worker
#!/bin/bash


cd /opt
wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tgz
tar -xvf Python-3.6.3.tgz
cd Python-3.6.3/
./configure
make
sudo apt-get install zlib1g-dev
make install
python3.6 -V

cd /root
sudo pip3 install --upgrade pip
sudo apt-get update
sudo apt-get install git-core

git clone https://github.com/kubernetes-sigs/kubespray.git

cd /root/kubespray

sudo pip3 install -r requirements.txt

cp -rfp inventory/sample inventory/case-muhammet-ozekli.abc

echo "dont forget updating kubernetes cluster name!!!!!!!"

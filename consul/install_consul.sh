#!/bin/bash

IP=$1

apt-get update -y
apt-get install unzip gnupg2 curl wget -y



export VER="1.8.4"
wget https://releases.hashicorp.com/consul/${VER}/consul_${VER}_linux_amd64.zip
apt update
apt install unzip
unzip consul_${VER}_linux_amd64.zip
chmod +x consul
sudo mv consul /usr/local/bin/
consul  version
groupadd --system consul
useradd -s /sbin/nologin --system -g consul consul
mkdir -p /var/lib/consul
chown -R consul:consul /var/lib/consul
chmod -R 775 /var/lib/consul

echo "[Unit]
Description=Consul Service Discovery Agent
Documentation=https://www.consul.io/
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=consul
Group=consul
ExecStart=/usr/local/bin/consul agent -server -ui \
        -advertise=$IP \
        -bind=$IP \
        -data-dir=/var/lib/consul \
        -node=consul-01 \
        -config-dir=/etc/consul.d

ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier=consul

[Install]
WantedBy=multi-user.target


" > /etc/systemd/system/consul.service

#ENC = "consul keygen"

mkdir /etc/consul.d/
chown -R consul:consul /etc/consul.d
echo '{
"bootstrap": true,
"server": true,
"log_level": "DEBUG",
"enable_syslog": true,
"datacenter": "server1",
"addresses" : {
"http": "0.0.0.0"
},
"bind_addr": "'"$IP"'",
"node_name": "'"$IP"'",
"data_dir": "/var/lib/consul",
"acl_datacenter": "server1",
"acl_default_policy": "allow",
"encrypt": "'"$(consul keygen)"'"
}
' >  /etc/consul.d/config.json

systemctl start consul
systemctl enable consul


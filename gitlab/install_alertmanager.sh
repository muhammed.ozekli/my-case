#!/bin/bash

#alertmanager --web.listen-address=http://localhost:9876
#alertmanager --web.listen-address=http://:9876


sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Alertmanager User" alertmanager
sudo mkdir /etc/alertmanager
sudo mkdir /etc/alertmanager/template
sudo mkdir -p /var/lib/alertmanager/data
sudo chown -R alertmanager:alertmanager /etc/alertmanager
sudo chown -R alertmanager:alertmanager /var/lib/alertmanager
sudo wget https://github.com/prometheus/alertmanager/releases/download/v0.12.0/alertmanager-0.12.0.linux-amd64.tar.gz
sudo tar xvzf alertmanager-0.12.0.linux-amd64.tar.gz
sudo cp alertmanager-0.12.0.linux-amd64/alertmanager /usr/local/bin/
sudo cp alertmanager-0.12.0.linux-amd64/amtool /usr/local/bin/
sudo chown alertmanager:alertmanager /usr/local/bin/alertmanager
sudo chown alertmanager:alertmanager /usr/local/bin/amtool


echo "creating /etc/alertmanager/alertmanager.yml"
echo "global:
  resolve_timeout: 1m
  slack_api_url: 'https://hooks.slack.com/services/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'

route:
  receiver: 'slack-notifications'

receivers:
- name: 'slack-notifications'
  slack_configs:
  - channel: '#monitoring-instances'
    send_resolved: true

" > /etc/alertmanager/alertmanager.yml


echo "creating /etc/systemd/system/alertmanager.service"

echo "[Unit]
Description=Prometheus Alertmanager Service
Wants=network-online.target
After=network.target

[Service]
User=alertmanager
Group=alertmanager
Type=simple
ExecStart=/usr/local/bin/alertmanager \
    --config.file /etc/alertmanager/alertmanager.yml \
    --storage.path /var/lib/alertmanager/data \
	--web.listen-address=:9094
Restart=always

[Install]
WantedBy=multi-user.target

" > /etc/systemd/system/alertmanager.service


sudo systemctl daemon-reload
sudo systemctl start alertmanager
sudo systemctl enable alertmanager
sudo systemctl status alertmanager


sudo echo "global:
  scrape_interval: 10s

rule_files:
  - /etc/prometheus/prometheus.rules
# Alerting specifies settings related to the Alertmanager
alerting:
  alertmanagers:
    - static_configs:
      - targets:
        # Alertmanager's default port is 9093
        - localhost:9094

scrape_configs:
  - job_name: 'externalprometheus'
    scrape_interval: 15s
    static_configs:
      - targets:
#        - '192.168.0.15:30909'
        - '192.168.0.13:9093'
        labels:
          node: externalPrometheus

  - job_name: 'consul'
    honor_labels: true
    metrics_path: '/federate'
    consul_sd_configs:
      - server: '192.168.0.13:8500'
        services:
        - 'prometheus'
    params:
      match[]:
        - '{__name__=~".+"}'
        - '{__name__=~"^job:.*"}'
        - '{job="prometheus"}'
        - '{job="node"}'
        - '{__name__="server_labels"}'
    relabel_configs:
    - source_labels: [__meta_consul_tags]
      regex: '.*,(dev|prod),.*'
      replacement: '${1}'
      target_label: env

" > /etc/prometheus/prometheus.yml

sudo echo "groups:
- name: demo alert
  rules:
  - alert: KubernetesPodNotHealthy
    expr: min_over_time(sum by (namespace, pod) (kube_pod_status_phase{phase=~"Pending|Unknown|Failed"})[1h:]) > 0
    for: 0m
    labels:
      severity: critical
    annotations:
      summary: Kubernetes Pod not healthy (instance {{ $labels.instance }})

  - alert: PodRestart
    expr:  rate(kube_pod_container_status_restarts_total[1m]) > 0
    annotations:
     description: '{{ $labels.pod }} has been restarted in namespace {{ $labels.namespace }}'
    labels:
       severity: 'critical'

" > /etc/prometheus/prometheus.rules
sudo chown prometheus:prometheus /etc/prometheus/prometheus.rules
sudo systemctl restart prometheus.service


rm alertmanager-0.12.0.linux-amd64.tar.gz
rm -rf alertmanager-0.12.0.linux-amd64
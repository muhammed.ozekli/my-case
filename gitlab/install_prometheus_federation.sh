#!/bin/bash
TARGET=${1}

echo "downloading the prometheus installation tar file"
wget https://github.com/prometheus/prometheus/releases/download/v2.8.1/prometheus-2.8.1.linux-amd64.tar.gz
sudo useradd --no-create-home --shell /bin/false prometheus
sudo mkdir /etc/prometheus
sudo mkdir /var/lib/prometheus
sudo chown prometheus:prometheus /etc/prometheus
sudo chown prometheus:prometheus /var/lib/prometheus
tar -xvzf prometheus-2.8.1.linux-amd64.tar.gz
mv prometheus-2.8.1.linux-amd64 prometheuspackage

sudo cp prometheuspackage/prometheus /usr/local/bin/
sudo cp prometheuspackage/promtool /usr/local/bin/
sudo chown prometheus:prometheus /usr/local/bin/prometheus
sudo chown prometheus:prometheus /usr/local/bin/promtool
sudo cp -r prometheuspackage/consoles /etc/prometheus
sudo cp -r prometheuspackage/console_libraries /etc/prometheus
sudo chown -R prometheus:prometheus /etc/prometheus/consoles
sudo chown -R prometheus:prometheus /etc/prometheus/console_libraries
#vim /etc/prometheus/prometheus.yml
echo "creating prometheus yaml file"
sudo echo "global:
  scrape_interval: 10s

scrape_configs:
  - job_name: 'selfmonitoring'
    scrape_interval: 15s

#    honor_labels: true
#    metrics_path: '/federate'

#    params:
#      'match[]':
#        - '{job=prometheus}'
#        - '{__name__=~job:.*}'

    static_configs:
      - targets:
#        - '192.168.0.15:30909'
        - '"$TARGET"'
        labels:
          node: primaryPrometheus

" > /etc/prometheus/prometheus.yml

#sudo cp prometheus.yml /etc/prometheus/prometheus.yml
#sudo cat /etc/prometheus/prometheus.yml

sudo chown prometheus:prometheus /etc/prometheus/prometheus.yml
#vim /etc/systemd/system/prometheus.service

echo "creating prometheus service"

sudo echo "[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
--config.file /etc/prometheus/prometheus.yml \
--storage.tsdb.path /var/lib/prometheus/ \
--web.console.templates=/etc/prometheus/consoles \
--web.console.libraries=/etc/prometheus/console_libraries \
--web.listen-address=:9093

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/prometheus.service
#sudo cp prometheus.service /etc/systemd/system/prometheus.service
#sudo cat /etc/systemd/system/prometheus.service

sudo systemctl daemon-reload
sudo systemctl enable prometheus
sudo systemctl start prometheus
sudo systemctl status prometheus
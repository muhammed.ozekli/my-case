import json
import datetime
import sys
from datetime import date

def openjson():
  a_file = open("prometheus_service.json", "r", encoding="utf8")
  json_object = json.load(a_file)
  a_file.close()
  return json_object

def prometheusmod(ip, port):
  json_object = openjson()
  json_object['Address'] = ip
  json_object['Port'] = port

#  bulksize = int(bulksize)
#  hardlimit = bool(hardlimit == "true")
#  size = int(size)

  return json_object

def main():

  address= sys.argv[1]
  port = sys.argv[2]
  port = int(port)

  #nowformat = '{0:%Y-%m-%dT%H:%M:%S.000Z}'.format(datetime.now())
  #print(nowformat)

  #givinIP = "192.168.0.14"
  #givinPort = 9093

  json_object = prometheusmod(address, port)

  print(json_object)

  a_file = open("prometheus_service.json", "w+")
  a_file.write(json.dumps(json_object, indent=2, ensure_ascii=False))
  a_file.close()

if __name__== "__main__":
    main()
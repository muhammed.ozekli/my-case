#!/bin/bash
TOKEN = ${1}

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E apt-get install gitlab-runner -y

sudo gitlab-runner register \
  --non-interactive \
  --url "http://192.168.0.14/" \
  --registration-token '"'$TOKEN'"' \
  --executor "shell" \
#  --docker-image alpine:latest \
  --description "gitlabLocalrunner" \
  --tag-list "gitlablocal" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
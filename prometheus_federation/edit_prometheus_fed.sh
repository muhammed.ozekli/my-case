#!/bin/bash
TARGET=${1}

echo "editing prometheus yaml file"
sudo echo "global:
  scrape_interval: 10s

scrape_configs:
  - job_name: 'externalprometheus'
    scrape_interval: 15s
    static_configs:
      - targets:
#        - '192.168.0.15:30909'
        - '"$TARGET"'
        labels:
          node: externalPrometheus

  - job_name: 'consul'
    honor_labels: true
    metrics_path: '/federate'
    consul_sd_configs:
      - server: '"$TARGET":8500'
        services:
        - 'prometheus'
    params:
      match[]:
        - '{__name__=~".+"}'
        - '{__name__=~"^job:.*"}'
        - '{job="prometheus"}'
        - '{job="node"}'
        - '{__name__="server_labels"}'
    relabel_configs:
    - source_labels: [__meta_consul_tags]
      regex: '.*,(dev|prod),.*'
      replacement: '${1}'
      target_label: env


" > /etc/prometheus/prometheus.yml

systemctl restart prometheus.service